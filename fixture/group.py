class GroupHelper:

    def __init__(self, app):
        self.app = app

    def open_groups_page(self):
        webdriver = self.app.webdriver
        webdriver.find_element_by_link_text("groups").click()

    def create(self, group):
        webdriver = self.app.webdriver
        self.open_groups_page()
        # init group creation
        webdriver.find_element_by_name("new").click()
        self.fill_group_form(group)
        # submit group
        webdriver.find_element_by_name("submit").click()
        self.return_to_group_page()

    def fill_group_form(self, group):
        webdriver = self.app.webdriver
        self.change_field_value("group_name", group.name)
        self.change_field_value("group_header", group.header)
        self.change_field_value("group_footer", group.footer)

    def change_field_value(self, field_name, text):
        webdriver = self.app.webdriver
        if text is not None:
            webdriver.find_element_by_name(field_name).click()
            webdriver.find_element_by_name(field_name).clear()
            webdriver.find_element_by_name(field_name).send_keys(text)

    def select_first_group(self):
        webdriver = self.app.webdriver
        webdriver.find_element_by_name("selected[]").click()

    def delete_first_group(self):
        webdriver = self.app.webdriver
        self.open_groups_page()
        self.select_first_group()
        # submit group deletion
        webdriver.find_element_by_name("delete").click()
        # return to group page
        self.return_to_group_page()

    def modify_first_group(self, new_group_data):
        webdriver = self.app.webdriver
        self.open_groups_page()
        self.select_first_group()
        # open modification form
        webdriver.find_element_by_name("edit").click()
        # fill group form
        self.fill_group_form(new_group_data)
        # submit group modification
        webdriver.find_element_by_name("update").click()
        self.return_to_group_page()

    def return_to_group_page(self):
        webdriver = self.app.webdriver
        webdriver.find_element_by_link_text("group page").click()
