from selenium.webdriver.chrome.webdriver import WebDriver
from fixture.session import SessionHelper
from fixture.group import GroupHelper


class Application:

    def __init__(self):
        self.webdriver = WebDriver()
        self.webdriver.implicitly_wait(60)
        self.session = SessionHelper(self)
        self.group = GroupHelper(self)

    def open_home_page(self):
        webdriver = self.webdriver
        webdriver.get("http://localhost:8081/addressbook/")

    def destroy(self):
        self.webdriver.quit()
